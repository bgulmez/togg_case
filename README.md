# togg_case

poi.proto dosyasını yeniden generate etmek için kullaanılır.
Generated :  protoc --dart_out=grpc:lib/network/generated -Iprotos protos/poi.proto

controller
    Viewlerin güncellenmesini yönetilmesini sağlayan controller lar mevcut.

entity
    Servislerden gelen cevabı kullanmak için parse edip yazdığım classlar. Obje olarak model de
     tutup sayfalarda kullanıyorum.

manager
    Api ve olsaydı local db bağlantılarını yapıldığı yer. view den gelen istekler controller
     tarafından işlenip servise gitme ihtiyacı varsa managera gelir. manager servise gidip gelen
     responsu entity'ye göre parse edip modele yazar. controllerda model den veriyi okuyupkullanır.

model
    entity olarak verileri tuttuğum kısım. manager modele yazar controller modelden okur.
    ihtiyaç dahilinde sayfalar arası geçişte veri taşıma olarak kullanılabilir

network
    Grpc Generated dosyaları ve servislerin bulunduğu klasör. abstract Base serviste oluşturulan
    client channel authentication ve poi locator serviste kullanılır.

route
    bind işlemi ve navigation işlemi yapısı burada tutulur. sayfaların tanımları yapılır.

View
    Ekranların bulunduğu  klasör

Uygulama ilk açıldığında main dart ta firebase crashlytics ve analytics ayarları yapılıp
login ekranı açılır. Login olduktan sonra home ekranı açılıp map oluşturulur. poi locator servisi
çağırılıp sonucunda gelen ResponseStream dinlenerek  entity çevirimi yapılıp streamController a eklenir
controllerdan managerda ki listen fonksiyonu çağırılıp stream dinlenmeye başlanır. gelen cevaplar
map e marker olarak eklenir.  marker a tıklayınca animasyon ile yaklaşıp bottom dialog açılır.
bottom dialogtan favori seçimi, favoriden çıkarma, linki açma gibi işlemler yapılır.
sağ üst köşeden favorilerim sayfasına gidilip favoriye eklenenler listelenir.

