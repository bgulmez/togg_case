import 'package:get/get.dart';
import 'package:togg_case/route/binding/favorite_binding.dart';
import 'package:togg_case/route/binding/home_binding.dart';
import 'package:togg_case/route/binding/login_binding.dart';
import 'package:togg_case/route/routers.dart';
import 'package:togg_case/view/favorite_view.dart';
import 'package:togg_case/view/home_view.dart';
import 'package:togg_case/view/login_view.dart';

mixin Pages {
  static List<GetPage> getPages = [
    GetPage(name: Routers.login, page: () => const LoginView(), binding: LoginBinding()),
    GetPage(name: Routers.home, page: () => const HomeView(), binding: HomeBinding()),
    GetPage(name: Routers.favorite, page: () => const FavoriteView(), binding: FavoriteBinding()),
  ];
}
