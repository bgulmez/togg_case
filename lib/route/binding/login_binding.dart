import 'package:get/get.dart';
import 'package:togg_case/controller/login_controller.dart';
import 'package:togg_case/manager/auth_manager.dart';
import 'package:togg_case/model/authentication_model.dart';
import 'package:togg_case/network/service/authentication_service.dart';


class LoginBinding extends Bindings {
  @override
  void dependencies() {

    Get.put<AuthenticationModel>(AuthenticationModel(), permanent: true);
    Get.put<AuthenticationService>(AuthenticationService(Get.find<AuthenticationModel>()));
    Get.put<AuthManager>(AuthManager(Get.find<AuthenticationService>(), Get.find<AuthenticationModel>()));

    Get.put<LoginController>(LoginController(Get.find<AuthManager>()));
  }
}
