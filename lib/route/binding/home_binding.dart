import 'package:get/get.dart';
import 'package:togg_case/controller/home_controller.dart';
import 'package:togg_case/manager/poi_locator_manager.dart';
import 'package:togg_case/model/authentication_model.dart';
import 'package:togg_case/model/poi_locator_model.dart';
import 'package:togg_case/network/service/poi_locator_service.dart';


class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<PoiLocatorModel>(PoiLocatorModel(),permanent: true);
    Get.put<PoiLocatorService>(PoiLocatorService(Get.find<AuthenticationModel>()));
    Get.put<PoiLocatorManager>(PoiLocatorManager(Get.find<PoiLocatorService>()));
    Get.put<HomeController>(HomeController(Get.find<PoiLocatorManager>(),Get.find<PoiLocatorModel>()));
  }
}
