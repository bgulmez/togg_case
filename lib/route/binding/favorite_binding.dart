import 'package:get/get.dart';
import 'package:togg_case/controller/favorite_controller.dart';
import 'package:togg_case/model/poi_locator_model.dart';


class FavoriteBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<FavoriteController>(FavoriteController(Get.find<PoiLocatorModel>()));
  }
}
