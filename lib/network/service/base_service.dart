import 'package:grpc/grpc.dart';
import 'package:togg_case/model/authentication_model.dart';


abstract class BaseService {
  late final ClientChannel clientChannel;
  final AuthenticationModel _authenticationModel;

  BaseService(this._authenticationModel) {
    clientChannel =  ClientChannel(
      "flutterassessment.togg.cloud",
      port: 80,
      options: const ChannelOptions(credentials: ChannelCredentials.insecure()),
    );
  }

  Map<String, String> getHeaders({bool isAuthenticated = true}) {
    final Map<String, String> map = {}; /// başka header varsa buraya eklenecek
    if(isAuthenticated && _authenticationModel.authenticationInfo!=null && _authenticationModel.authenticationInfo!.token!=null){
      map["Authorization"] = "Bearer " + _authenticationModel.authenticationInfo!.token!;
    }
    return map;
  }
}
