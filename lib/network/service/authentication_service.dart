import 'package:togg_case/model/authentication_model.dart';
import 'package:togg_case/network/generated/poi.pbgrpc.dart';
import 'package:togg_case/network/service/base_service.dart';

class AuthenticationService extends BaseService {
  late final AuthenticationClient authenticationClient;
  final AuthenticationModel _authenticationModel;

  AuthenticationService(this._authenticationModel) :super(_authenticationModel) {
    authenticationClient = AuthenticationClient(clientChannel);
  }

  Future<LoginReply> login(LoginRequest request) async {
    return await authenticationClient.login(request);
  }
}
