import 'package:grpc/grpc.dart';
import 'package:togg_case/model/authentication_model.dart';
import 'package:togg_case/network/generated/poi.pbgrpc.dart';
import 'package:togg_case/network/service/base_service.dart';

class PoiLocatorService extends BaseService {
  late final PoiLocatorClient poiLocatorClient;
  final AuthenticationModel _authenticationModel;

  PoiLocatorService(this._authenticationModel) : super(_authenticationModel) {
    poiLocatorClient = PoiLocatorClient(clientChannel);
  }

  Future<ResponseStream<PoiReply>> getPoiLocator() async {
    ResponseStream<PoiReply> poiReply =
        poiLocatorClient.getPois(PoiRequest(), options: CallOptions(metadata: getHeaders(isAuthenticated: true)));
    return poiReply;
  }
}
