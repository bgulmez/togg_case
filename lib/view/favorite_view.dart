import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:togg_case/controller/favorite_controller.dart';
import 'package:togg_case/entity/poi_locator.dart';
import 'package:togg_case/theme/custom_color.dart';

class FavoriteView extends GetView<FavoriteController> {
  const FavoriteView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(),
      body: GetBuilder<FavoriteController>(
        id: "favoriteList",
        builder: (_controller) {
          return ListView.builder(
            shrinkWrap: true,
            physics: const AlwaysScrollableScrollPhysics(),
            itemCount: _controller.favoritePoiLocatorList.length,
            itemBuilder: (_, index) {
              final PoiLocator poi = _controller.favoritePoiLocatorList[index];
              return Container(
                margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  border: Border.all(color: Colors.black45),
                  borderRadius: const BorderRadius.all(Radius.circular(12)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      poi.name!,
                      style: const TextStyle(fontSize: 22),
                    ),
                    TextButton(onPressed: () => _controller.deleteButtonClick(poi), child: const Text("Kaldır")),
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
      systemOverlayStyle: SystemUiOverlayStyle.light,
      bottomOpacity: 0.0,
      automaticallyImplyLeading: true,
      backgroundColor: CustomColors.primary,
      centerTitle: true,
      titleSpacing: 0.0,
      title: const Padding(
        padding: EdgeInsets.all(50),
        child: Text("Favoriler"),
      ),
    );
  }
}
