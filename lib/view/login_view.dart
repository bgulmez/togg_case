import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:togg_case/constant/strings.dart';
import 'package:togg_case/controller/login_controller.dart';
import 'package:togg_case/theme/custom_color.dart';
import 'package:togg_case/theme/styles.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(horizontal: 24),
            margin: const EdgeInsets.only(top: 100),
            child: Column(
              children: <Widget>[
                Image.asset(
                  'assets/img/logo.png',
                  height: 30,
                ),
                const SizedBox(height: 30),
                Form(
                  key: controller.formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Column(
                    children: [
                      buildUserNameTextFormField(node),
                      const SizedBox(height: 10),
                      buildPswTextFormField(),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                buildContinueButton(),
              ],
            ),
          ),
        ),
      ),
      // backgroundColor: bgColor,
    );
  }

  TextFormField buildUserNameTextFormField(FocusScopeNode node) {
    return TextFormField(
      controller: controller.userNameController,
      focusNode: controller.userNameFocusNode,
      inputFormatters: [
        LengthLimitingTextInputFormatter(8),
      ],
      validator: (value) => value == null || value.isEmpty ? Strings.usernameWarning : null,
      decoration: InputDecoration(
        hintText: Strings.usernameIdNumber,
        hintStyle: Style.regularGrey3,
        border: InputBorder.none,
        prefixIcon: const Icon(
          Icons.person,
          color: CustomColors.primary,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: CustomColors.primary),
          borderRadius: BorderRadius.circular(5),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(5),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: CustomColors.primary),
          borderRadius: BorderRadius.circular(5),
        ),
      ),
      textInputAction: TextInputAction.next,
      onEditingComplete: () => node.nextFocus(),
    );
  }

  TextFormField buildPswTextFormField() {
    return TextFormField(
      obscureText: true,
      controller: controller.passwordController,
      focusNode: controller.passwordFocusNode,
      textInputAction: TextInputAction.send,
      keyboardType: TextInputType.text,
      validator: (value) => value == null || value.isEmpty ? Strings.passWarning : null,
      decoration: InputDecoration(
        hintText: 'Parola',
        hintStyle: Style.regularGrey3,
        border: InputBorder.none,
        prefixIcon: const Icon(
          Icons.lock_outline,
          color: CustomColors.primary,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: CustomColors.primary),
          borderRadius: BorderRadius.circular(5),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red),
          borderRadius: BorderRadius.circular(5),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: CustomColors.primary),
          borderRadius: BorderRadius.circular(5),
        ),
      ),
      inputFormatters: [LengthLimitingTextInputFormatter(8)],
      onFieldSubmitted: (val) {
        controller.login();
      },
    );
  }

  GetBuilder<LoginController> buildContinueButton() {
    return GetBuilder<LoginController>(
      id: "ContinueButton",
      builder: (controller) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ElevatedButton(
              onPressed: controller.buttonEnabled ? controller.login : null,
              style: ElevatedButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20),
                padding: const EdgeInsets.symmetric(vertical: 15),
                onSurface: CustomColors.primary,
              ),
              child: Text(
                Strings.login,
                style: Style.regularWhite2,
              ),
            ),
          ],
        );
      },
    );
  }
}
