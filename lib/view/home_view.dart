import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:get/get.dart';
import 'package:latlong2/latlong.dart';
import 'package:togg_case/controller/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Stack(
          children: [
            GetBuilder<HomeController>(
              id: "map",
              builder: (_controller) {
                return FlutterMap(
                  mapController: _controller.mapController,
                  options: MapOptions(
                    maxZoom: 18,
                    bounds: LatLngBounds(LatLng(42.35854392, 25.87280273), LatLng(36.66841892, 45.29663086)),
                    center: LatLng(0, 0),
                  ),
                  layers: [
                    TileLayerOptions(
                      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c'],
                      attributionBuilder: (_) {
                        return const Text("© OpenStreetMap contributors");
                      },
                    ),
                    MarkerLayerOptions(markers: _controller.markers)
                  ],
                );
              },
            ),
            Container(
              margin: const EdgeInsets.only(top: 50),
              alignment: Alignment.topRight,
              child: InkWell(
                onTap: () => controller.favoritesPage(),
                child: Container(
                  padding: const EdgeInsets.all(8),
                  decoration: const BoxDecoration(color: Colors.white),
                  child: const Text("Favorites"),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
