import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:togg_case/entity/poi_locator.dart';
import 'package:togg_case/manager/analytics_manager.dart';
import 'package:url_launcher/url_launcher.dart';

class CustomDialog {
  ///Singleton
  CustomDialog._privateConstructor();

  static final CustomDialog _instance = CustomDialog._privateConstructor();

  static CustomDialog get instance => _instance;

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: Row(
        children: [
          const CircularProgressIndicator(),
          Container(margin: const EdgeInsets.only(left: 7), child: const Text("Yükleniyor...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showInfoDialog(BuildContext context, String title, String message) {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text(title),
              content: Text(message),
              actions: [
                TextButton(
                  onPressed: () => Navigator.pop(context, 'Tamam'),
                  child: const Text('Tamam'),
                ),
              ],
            ));
  }

  void showSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message)),
    );
  }

  void showBottomBar(PoiLocator poiLocator, {required void Function(bool isFavorite) favoriteClick}) {
    Get.bottomSheet(Container(
        padding: const EdgeInsets.all(10),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
        ),
        height: MediaQuery.of(Get.context!).size.height * 0.1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Text(
                  poiLocator.name!,
                  style: const TextStyle(fontSize: 22),
                ),
                const Spacer(),
                InkWell(
                  onTap: () {
                    AnalyticsManager.addEvent(Events.markerClick.name);
                    launch(poiLocator.website!);
                  },
                  child: Text(poiLocator.website!, style: const TextStyle(fontSize: 14, color: Colors.blue)),
                )
              ],
            ),
            Text(poiLocator.openNow! ? "OPEN" : "CLOSE"),
            StarButton(
              isStarred: poiLocator.isFavorite,
              valueChanged: favoriteClick,
            )
          ],
        )));
  }
}
