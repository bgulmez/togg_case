import 'package:togg_case/entity/poi_locator.dart';

class PoiLocatorModel {
  List<PoiLocator> poiLocatorList = [];

  void addOrRemoveFavorite(PoiLocator data) {
    for (PoiLocator poiLocator in poiLocatorList) {
      if (poiLocator.id == data.id) {
        poiLocator.isFavorite = !poiLocator.isFavorite;
        break;
      }
    }
  }
}
