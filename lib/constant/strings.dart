mixin Strings {

  static var usernameWarning ='Kullanıcı Adı boş olamaz!';
  static var usernameIdNumber = 'Kullanıcı Adı / TCKN';
  static var passWarning ='Parola boş olamaz!';
  static var yes   = "Evet";
  static var no = "Hayır";
  static String login = "GİRİŞ";
}
