
class PoiLocator {
  int? id;
  double? lat;
  double? lon;
  String? name;
  bool? openNow;
  String? website;
  bool isFavorite;

  PoiLocator(this.id, this.lat, this.lon, this.name, this.openNow, this.website,this.isFavorite);
}
