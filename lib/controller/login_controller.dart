import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:grpc/grpc.dart';
import 'package:togg_case/manager/analytics_manager.dart';
import 'package:togg_case/manager/auth_manager.dart';
import 'package:togg_case/route/routers.dart';
import 'package:togg_case/view/widget/custom_dialog.dart';

class LoginController extends GetxController {
  final AuthManager authManager;

  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  FocusNode userNameFocusNode = FocusNode();
  FocusNode passwordFocusNode = FocusNode();

  bool buttonEnabled = false;

  LoginController(this.authManager);

  @override
  void onReady() {
    super.onReady();
    passwordController.addListener(passwordChange);
  }

  Future login() async {
    try {
      final FormState form = formKey.currentState!;
      if (!form.validate()) {
        CustomDialog.instance.showSnackBar(Get.context!, "Lütfen gerekli alanları doldurunuz!");
        return;
      }
      CustomDialog.instance.showLoaderDialog(Get.context!);
      await authManager.login(userNameController.text, passwordController.text);
      AnalyticsManager.addEvent(Events.loginSuccess.name);
      Get.back(); // dialog kapama
      Get.offAllNamed(Routers.home);
    } on GrpcError catch (e) {
      Get.back();
      CustomDialog.instance.showInfoDialog(Get.context!, "Error", e.message.toString());
    }
  }

  void passwordChange() {
    if (userNameController.text.isNotEmpty && passwordController.text.isNotEmpty) {
      buttonEnabled = true;
    } else {
      buttonEnabled = false;
    }
    update(["ContinueButton"]);
  }
}
