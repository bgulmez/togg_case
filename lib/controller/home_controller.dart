import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:get/get.dart';
import 'package:latlong2/latlong.dart';
import 'package:togg_case/manager/analytics_manager.dart';
import 'package:togg_case/manager/poi_locator_manager.dart';
import 'package:togg_case/entity/poi_locator.dart';
import 'package:togg_case/model/poi_locator_model.dart';
import 'package:togg_case/route/routers.dart';
import 'package:togg_case/view/widget/custom_dialog.dart';

class HomeController extends GetxController with GetTickerProviderStateMixin {
  final PoiLocatorManager _poiLocatorManager;
  final PoiLocatorModel _poiLocatorModel;

  CancelListening? _cancelListening;
  late MapController mapController;

  List<Marker> markers = [];
  List<PoiLocator> poiLocatorList = [];

  HomeController(this._poiLocatorManager, this._poiLocatorModel);

  @override
  void onInit() {
    super.onInit();
    mapController = MapController();
  }

  @override
  void onReady() {
    super.onReady();
    getPoi();
  }

  @override
  void onClose() {
    super.onClose();
    if (_cancelListening != null) {
      _cancelListening!.call();
    }
    _poiLocatorManager.close();
  }

  Future getPoi() async {
    markers.clear();
    CustomDialog.instance.showLoaderDialog(Get.context!);
    await _poiLocatorManager.getPoiLocator();
    Get.back();
    _cancelListening = await _poiLocatorManager.listen((data) {
      poiLocatorList.add(data);
      markers.add(getMarker(data));
      update(["map"]);
    });
  }

  Marker getMarker(PoiLocator data) {
    return Marker(
      point: LatLng(data.lat!, data.lon!),
      key: Key(data.id.toString()),
      height: 50,
      width: 50,
      builder: (context) {
        return GestureDetector(
          onTap: () {
            AnalyticsManager.addEvent(Events.markerClick.name);
            zoomMarkers(data.lat!, data.lon!);
            CustomDialog.instance.showBottomBar(data, favoriteClick: (bool isFavorite) {
              String eventName = isFavorite ? Events.addFavorite.name : Events.removeFavorite.name;
              AnalyticsManager.addEvent(eventName, parameters: {"MarkerName": data.name});
              _poiLocatorModel.addOrRemoveFavorite(data);
              data.isFavorite = isFavorite;
            });
          },
          child: _getMarker(data.name),
        );
      },
    );
  }

  Widget _getMarker(String? name) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.yellow,
        border: Border.all(color: Colors.white.withOpacity(0.5), width: 2.0),
        borderRadius: const BorderRadius.all(Radius.elliptical(100, 100)),
      ),
      child: Align(
        child: Text(
          name!,
        ),
      ),
    );
  }

  void favoritesPage() {
    AnalyticsManager.addEvent(Events.favoritesPage.name);
    _poiLocatorModel.poiLocatorList = poiLocatorList;
    Get.toNamed(Routers.favorite)!.then((value) {
      poiLocatorList = _poiLocatorModel.poiLocatorList;
      markers.clear();
      for (PoiLocator poi in poiLocatorList) {
        markers.add(getMarker(poi));
      }
      update(["map"]);
    });
  }

  void zoomMarkers(double lat, double lon) {
    _animatedMapMove(LatLng(lat, lon), 10);
  }

  void _animatedMapMove(LatLng destLocation, double destZoom) {
    final _latTween = Tween<double>(begin: mapController.center.latitude, end: destLocation.latitude);
    final _lngTween = Tween<double>(begin: mapController.center.longitude, end: destLocation.longitude);
    final _zoomTween = Tween<double>(begin: mapController.zoom, end: destZoom);

    final controller = AnimationController(duration: const Duration(milliseconds: 1200), vsync: this);
    final Animation<double> animation = CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn);

    controller.addListener(() {
      mapController.move(LatLng(_latTween.evaluate(animation), _lngTween.evaluate(animation)), _zoomTween.evaluate(animation));
    });

    animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.dispose();
      } else if (status == AnimationStatus.dismissed) {
        controller.dispose();
      }
    });

    controller.forward();
  }
}
