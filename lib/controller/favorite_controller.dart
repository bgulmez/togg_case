import 'package:get/get.dart';
import 'package:togg_case/entity/poi_locator.dart';
import 'package:togg_case/manager/analytics_manager.dart';
import 'package:togg_case/model/poi_locator_model.dart';

class FavoriteController extends GetxController {
  final PoiLocatorModel _poiLocatorModel;

  List<PoiLocator> favoritePoiLocatorList = [];

  FavoriteController(this._poiLocatorModel);

  @override
  void onInit() {
    super.onInit();
    favoritePoiLocatorList = _poiLocatorModel.poiLocatorList.where((element) => element.isFavorite).toList();
  }

  void deleteButtonClick(PoiLocator poi) {
    AnalyticsManager.addEvent(Events.removeFavorite.name, parameters: {"MarkerName": poi.name});
    _poiLocatorModel.addOrRemoveFavorite(poi);
    favoritePoiLocatorList = _poiLocatorModel.poiLocatorList.where((element) => element.isFavorite).toList();
    update(["favoriteList"]);
  }
}
