import 'package:flutter/material.dart';
import 'package:togg_case/theme/custom_color.dart';

mixin Style {
  static TextStyle regularWhite1 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 20, color: Colors.white);
  static TextStyle regularWhite2 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 18, color: Colors.white);
  static TextStyle regularWhite3 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 16, color: Colors.white);
  static TextStyle regularWhite4 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 14, color: Colors.white);
  static TextStyle regularWhite5 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: Colors.white);

  static TextStyle mediumWhite1 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 20, color: Colors.white);
  static TextStyle mediumWhite2 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.white);
  static TextStyle mediumWhite3 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 16, color: Colors.white);
  static TextStyle mediumWhite4 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: Colors.white);
  static TextStyle mediumWhite5 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 12, color: Colors.white);

  static TextStyle h1White = const TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: Colors.white);
  static TextStyle h2White = const TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white);
  static TextStyle h3White = const TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white);
  static TextStyle h4White = const TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white);
  static TextStyle h5White = const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: Colors.white);

  static TextStyle h1Grey = const TextStyle(fontWeight: FontWeight.bold, fontSize: 24, color: CustomColors.grey);
  static TextStyle h2Grey = const TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: CustomColors.grey);
  static TextStyle h3Grey = const TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: CustomColors.grey);
  static TextStyle h4Grey = const TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: CustomColors.grey);
  static TextStyle h5Grey = const TextStyle(fontWeight: FontWeight.bold, fontSize: 16, color: CustomColors.grey);

  static TextStyle regularGrey1 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 20, color: CustomColors.grey);
  static TextStyle regularGrey2 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 18, color: CustomColors.grey);
  static TextStyle regularGrey3 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 16, color: CustomColors.grey);
  static TextStyle regularGrey4 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 14, color: CustomColors.grey);
  static TextStyle regularGrey5 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: CustomColors.grey);

  static TextStyle mediumGrey1 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 20, color: CustomColors.grey);
  static TextStyle mediumGrey2 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: CustomColors.grey);
  static TextStyle mediumGrey3 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 16, color: CustomColors.grey);
  static TextStyle mediumGrey4 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: CustomColors.grey);
  static TextStyle mediumGrey5 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 12, color: CustomColors.grey);

  static TextStyle regularBlack1 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 20, color: Colors.black);
  static TextStyle regularBlack2 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 18, color: Colors.black);
  static TextStyle regularBlack3 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 16, color: Colors.black);
  static TextStyle regularBlack4 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 14, color: Colors.black);
  static TextStyle regularBlack5 = const TextStyle(fontWeight: FontWeight.normal, fontSize: 12, color: Colors.black);

  static TextStyle mediumBlack1 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 20, color: Colors.black);
  static TextStyle mediumBlack2 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 18, color: Colors.black);
  static TextStyle mediumBlack3 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 16, color: Colors.black);
  static TextStyle mediumBlack4 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: Colors.black);
  static TextStyle mediumBlack5 = const TextStyle(fontWeight: FontWeight.w500, fontSize: 12, color: Colors.black);

  static TextStyle textButton = const TextStyle(fontWeight: FontWeight.w500, fontSize: 14, color: CustomColors.primary, decoration: TextDecoration.underline);

}
