import 'package:flutter/material.dart';

mixin CustomColors {
  static const Color primary = Color(0xFF68BECD);
  static const Color primaryDark = Color(0xFF277A89);
  static const Color secondary = Color(0xFFEEEEEF);

  static const Color grey = Color(0xFF6a6a6a);


}
