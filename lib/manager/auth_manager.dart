import 'package:togg_case/entity/authentication_info.dart';
import 'package:togg_case/model/authentication_model.dart';
import 'package:togg_case/network/generated/poi.pb.dart';
import 'package:togg_case/network/service/authentication_service.dart';

class AuthManager {
  final AuthenticationService _authService;
  final AuthenticationModel _authenticationModel;

  AuthManager(this._authService, this._authenticationModel);

  Future<void> login(String username, String password) async {
    LoginReply reply = await _authService.login(LoginRequest(username: username, password: password));
    _authenticationModel.authenticationInfo = AuthenticationInfo(reply.token);
  }
}
