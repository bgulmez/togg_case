import 'package:firebase_analytics/firebase_analytics.dart';

enum Events {
  loginSuccess,
  addFavorite,
  removeFavorite,
  linkClick,
  markerClick,
  favoritesPage,
}

class AnalyticsManager {
  static addEvent(String name, {Map<String, Object?>? parameters}) async {
    await FirebaseAnalytics.instance.logEvent(
      name: name,
      parameters: parameters,
    );
  }
}

extension IEvent on Events {
  String get name {
    switch (this) {
      case Events.loginSuccess:
        return "Login";
      case Events.addFavorite:
        return "AddFavorite";
      case Events.removeFavorite:
        return "RemoveFavorite";
      case Events.linkClick:
        return "ClickLink";
      case Events.markerClick:
        return "MarkerClick";
      case Events.favoritesPage:
        return "FavoritesPage";
    }
  }
}
