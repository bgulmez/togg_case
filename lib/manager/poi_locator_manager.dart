import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:grpc/service_api.dart';
import 'package:togg_case/entity/poi_locator.dart';
import 'package:togg_case/network/service/poi_locator_service.dart';

import '../network/generated/poi.pbgrpc.dart';

typedef CancelListening = void Function();

class PoiLocatorManager {
  final PoiLocatorService _poiLocatorService;

  PoiLocatorManager(this._poiLocatorService);

  late StreamController<PoiLocator> _streamController;
  late StreamSubscription<dynamic> _subscription;
  late StreamSubscription<PoiLocator> subscriptionCall;

  Future<void> getPoiLocator() async {
    try {
      _streamController = StreamController.broadcast();
      ResponseStream<PoiReply> poiStream = await _poiLocatorService.getPoiLocator();
      _subscription = poiStream.listen((value) {
        _streamController.add(PoiLocator(int.parse(value.id.toString()), value.lat, value.lon, value.name, value.openNow, value.website, false));
      });
    } on Exception catch (e) {
      debugPrint(e.toString());
    }
  }

  void close() {
    _subscription.cancel();
    _streamController.close();
  }

  Future<CancelListening> listen(void Function(PoiLocator data) onData) async {
    if (!_streamController.hasListener) {
      subscriptionCall = _streamController.stream.listen(onData);
    }
    return () {
      subscriptionCall.cancel();
    };
  }
}
