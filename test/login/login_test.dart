

import 'package:flutter_test/flutter_test.dart';
import 'package:togg_case/model/authentication_model.dart';
import 'package:togg_case/network/generated/poi.pbgrpc.dart';
import 'package:togg_case/network/service/authentication_service.dart';

void main(){
  final AuthenticationModel authenticationModel = AuthenticationModel();
  late AuthenticationService authenticationService;
  setUp((){
    authenticationService = AuthenticationService(authenticationModel);
  });

  test("LoginTest",() async{
    LoginReply reply = await authenticationService.login(LoginRequest(username: "Test",password:  "Togg"));
    expect(reply, isNotNull);
  });
}